`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    2017-04-04
// Design Name: 
// Module Name:    nor2
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: Double nor gate
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module nor2(
    input in00,
    input in01,
    input in10,
    input in11
    output out0,
    output out1
    );

wire o0 = !(in00 || in01);
wire o1 = !(in10 || in11);

assign out0 = o0;
assign out1 = o1;

endmodule
