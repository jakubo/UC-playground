`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    00:48:25 02/18/2017 
// Design Name: 
// Module Name:    bcd_2_2421
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: Verilog bcd to 2421 converter
//		Converts 4 bit bcd input to 4 bit 2421
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: See mapping file for
//		basys2 mapping where input is first 4 switches
//		and output is first 4 led's
//
//////////////////////////////////////////////////////////////////////////////////
module bcd_2_2421(
    input [3:0] bcd,
    output [3:0] o_2421
    );

wire A = bcd[3];
wire B = bcd[2];
wire C = bcd[1];
wire D = bcd[0];

assign o_2421[0] = bcd[0];
assign o_2421[1] = (!B && C) || (A && D) || (B && !C & D);
assign o_2421[2] = A || (B && C) || (B && !D);
assign o_2421[3] = A || (B && C) || (B && D);

endmodule
