"""Pulse generator for labjack U3.

Works pretty ok for up to ~500Hz.
"""
from time import sleep

from contextlib import closing

import u3


def start(freq_hz=1, duty_cycle=50, port=u3.FIO4, invert_port=None):
    """Starts infinite loop pulse generator with
    frequency ``freq_hz``, duty cycle ``duty_cycle``.

    ``port`` is the U3 output port to be used.

    U3 is expected to be available (closed).
    """
    cycle_length = 1.0 / freq_hz
    state_on_duration = (duty_cycle / 100) * cycle_length
    state_off_duration = cycle_length - state_on_duration

    with closing(u3.U3()) as dev:
        dev.configDigital(port)
        if invert_port is not None:
            dev.configDigital(invert_port)

        while True:
            dev.setFIOState(port, 1)
            if invert_port is not None:
                dev.setFIOState(invert_port, 0)
            sleep(state_on_duration)
            dev.setFIOState(port, 0)
            if invert_port is not None:
                dev.setFIOState(invert_port, 1)
            sleep(state_off_duration)

def side(dev, which, state):
    one, two = (u3.FIO7, u3.FIO6) if which == 'LEFT' else (u3.FIO4, u3.FIO5)
    dev.setFIOState(one, int(state))
    dev.setFIOState(two, int(not state))


def step(dev):
    side(dev, 'RIGHT', True);  side(dev, 'LEFT', True)
    side(dev, 'RIGHT', False); side(dev, 'LEFT', True)
    side(dev, 'RIGHT', False); side(dev, 'LEFT', False)
    side(dev, 'RIGHT', True);  side(dev, 'LEFT', False)


STEP_SEQ = ((1, 1), (0, 1), (0, 0), (1, 0))

def s(dev, direction):
    one, two = (u3.FIO4, u3.FIO5) if direction == 'LEFT' else (u3.FIO5, u3.FIO4)
    for one_val, two_val in STEP_SEQ:
        dev.setFIOState(one, one_val)
        dev.setFIOState(two, two_val)
