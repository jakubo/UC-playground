/*
 * File:   main.c
 * Author: jakubo
 *
 * Created on August 25, 2016, 11:50 PM
 *
 *
 * Measure temperature with LM335A via AD and display on 3 digit 7 segment.
 * For simplicity assumes positive temperatures.
 *
 */


#include <xc.h>

#pragma config FOSC = INTRCCLK
#pragma config WDTE = OFF
#pragma config PWRTE = ON
#pragma config MCLRE = ON
#pragma config CP = OFF
#pragma config CPD = OFF
#pragma config BOREN = ON
#pragma config IESO = ON
#pragma config FCMEN = ON

#define _XTAL_FREQ 4000000

#define NUM_DIGITS 3

#define SKIP_CYCLES 50


unsigned short adres = 0;
unsigned char cycle_count = 0;
short temperature = 0 ;

const char digit_map[] = {
    // mapping between digits and 7 segment display
    // value at each index is a digit for that index
    // for example digit_map[0] displays "0", digit_map[7] displays "7" etc.
    // 7 segment will be hooked to PORTC bits 0:6
    // so the value can be directly written there
    // bit 7 is ignored
    // pin-out for display:
    // --7--
    // |   |
    // 2   6
    // |   |
    // --1--
    // |   |
    // 3   5
    // |   |
    // --4--
    0b11000000,
    0b11111001,
    0b10100100,
    0b10110000,
    0b10011001,
    0b10010010,
    0b10000010,
    0b11111000,
    0b10000000,
    0b10010000
};

const char show_digit[] = {
    // B7 is first digit, B6 is second, B5 is third
    // Values below can be directly written to PORTB to activate chosen digit
    // first element means first digit active etc.
    0b10000000,
    0b01000000,
    0b00100000
};

void activate_digit(char digit_no){
    // activates digit # ``digit_no`` on display
    // ``digit_no`` must be 0, 1, 2
    PORTB = show_digit[digit_no];
}

void display_digit(char digit){
    // display ``digit`` on 7 segment led display.
    PORTC = digit_map[digit];
}

short lm355a_temperature(){
    // returns temperature calculated from adres.

    // ADC Reference = 5V
    // Each ADC reading is 5V/1024 (10 bit ADC @ 0-5V)
    // So each ADC unit = 0.00488V


    // LM335A outputs 2.98V@25C and 1C = 10mV
    // So Voltage @ 0C = 2.73           (2.98 - (25 * 0.01))
    // That is ADC reading @0C = 559    (2.73/(5/1024))


    // Scale things up so we can operate on integers (floating point is expensive)
    // We use 488 instead of 0.00488:
    //      multiply * 100 (1C = 10mV)
    //      multiply * 100 to stay outside of fractions
    //      We later divide by 100

    return ((adres - 559) * 488) / 100;

}

void main(void) {
    TRISB = 0;  // PORTB is now digital output (for switching digits)
    TRISC = 0;  // PORTC is now digital output (for displaying digits)

    TRISA = 0xff; // PORTA is input (for thermo sensor);
    ANSEL = 0xff; // PORT AN0 is analog (for thermo sensor);

    // Set all AD stuff in one go.
    // Bits in ADCON0 from left to right:
    //1 (ADFM) = right justified results in ADRES(H/L)
    //0 (VCFG) = VDD is reference voltage.
    //0000 (CHS<3:0>) = AN0 is input channel
    //1 (GO/DONE) = GO - start AD conversion cycle
    //1 (ADON) = ADC is enabled
    ADCON0 = 0b10000011;

    // Set ADCON<6:4> to 101 - sets conversion clock to Fosc/16
    ADCON1 |= 0b0101000;

    PEIE = 1;   // Enable peripheral interrupts.
    ADIE = 1;   // AD Interrupt on.
    ei();   //start interrupt machinery.
    GO = 1;

    while(1){
        // To avoid constant display flickering only update 
        // temperature every SKIP_CYCLE cycles.
        // "cycle" in this context is not cpu cycle but number
        // of passes through the main loop
        if(++cycle_count > SKIP_CYCLES){
            temperature = lm355a_temperature();
            cycle_count = 0;
        }

        char digits[NUM_DIGITS];
        digits[0] = temperature / 100;
        digits[1] = (temperature % 100) / 10;
        digits[2] = temperature % 10;

        for(char i=0; i<NUM_DIGITS; i++){
            // multiplex three digits
            activate_digit(i);
            display_digit(digits[i]);
            __delay_ms(4);
        }
    }
}


void interrupt isr(){
    //Handle interrupts

    // Only interested in AD interrupt
    if(ADIF){
        // Combine all 10 bits from into 16 bit adres (ADRESH+ADRESL)
        adres = ADRESH << 8;
        adres ^= ADRESL;
        if(adres > 999)adres = 999;
        ADIF = 0;   // Green light for another round.
        GO = 1;
    }
}
