/*
 * File:   main.c
 * Author: jakubo
 *
 * Created on August 23, 2016, 8:07 PM
 *
 * Count when pin state changes and display it on 7 segment led display
 *
 */


#include <xc.h>

#pragma config FOSC = INTRCCLK
#pragma config WDTE = OFF
#pragma config PWRTE = ON
#pragma config MCLRE = ON
#pragma config CP = OFF
#pragma config CPD = OFF
#pragma config BOREN = ON
#pragma config IESO = ON
#pragma config FCMEN = ON

#define _XTAL_FREQ 4000000


unsigned char counter = 0;

const char digit_map[] = {
    // mapping between digits and 7 segment display
    // value at each index is a digit for that index
    // for example digit_map[0] displays "0", digit_map[7] displays "7" etc.
    // 7 segment will be hooked to PORTC bits 0:6
    // so the value can be directly written there
    // bit 7 is ignored
    // pin-out for display:
    // --7--
    // |   |
    // 2   6
    // |   |
    // --1--
    // |   |
    // 3   5
    // |   |
    // --4--
    0b00111111,
    0b00000110,
    0b01011011,
    0b01001111,
    0b01100110,
    0b01101101,
    0b01111101,
    0b00000111,
    0b01111111,
    0b01101111
};

void display_digit(char digit){
    // display ``digit`` on 7 segment led display.
    PORTC = digit_map[digit];
}

void tick(){
    // Increase global ``counter``.
    // Only allow single digit numbers
    if(counter < 9){
        ++counter;
    }else{
        counter = 0;
    }
}

void main(void) {

    ANSEL = 0;  // Disable input buffers.
    ANSELH = 0;
    TRISC = 0;  // PORTC is now digital output.
    TRISA2 = 1; // RA2 is now input (for interrupt)

    CM2CON0 = 0;    // Disable analog comparator on interrupt pin.

    INTEDG = 1; // Interrupt on rising edge.

    PEIE = 1;   // Enable peripheral interrupts.
    INTE = 1;   // Enable external interrupt on RA2
    ei();       // Activate interrupt machinery


    while(1){
        // Just chill and wait for interrupt
    }
}

void interrupt isr()
{
    if(INTF){      // Check if pin change interrupt
        tick();
        display_digit(counter);
        INTF = 0;  // clean up interrupt bit
    }
}
