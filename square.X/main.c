/*
 * File:   main.c
 * Author: jakubo
 *
 * Created on August 21, 2016, 6:04 PM
 * 
 * 50% duty square generator
 * 
 * On PIC 12F509 with internal oscillator:
 * Generated ~ 100Hz when __delay_ms(4) was used.
 * Generated ~ 1kHz when 15 cycles loop was used.
 * 
 * I was running this on PIC 12F509 but it's probably simple/generic
 * enough to run on any low/mid range PIC.
 * 
 */


#include <xc.h>

#pragma config MCLRE = ON, CP = OFF, WDT = OFF, OSC = IntRC

#define _XTAL_FREQ 4000000

#define OUT_PORT    GPIObits.GP0

void main(void) {
    
    GPIO = 0b0000;  // all output off
    TRIS = 0;  // set GP as output
    
    for(;;){
        OUT_PORT = 0;
        //__delay_ms(4);
        for(int i=0; i<=15;i++);
        OUT_PORT = 1;
        //__delay_ms(4);
        for(int i=0; i<=15;i++);
    }
}
