// tic tac toe
// Some constraints:
// user always starts, user is always X, screen is not cleaned, every new move is appended
// user must always enter number (input is not sanitated
// Workings:
// user/computer contains user/computer moves. Each player can occupy fields 0-8
// for each occupied field the corresponding bit is set
// Computer move is random :)
////////////////////////////////////////////////////////////////////////
////////////////////////// How to play /////////////////////////////////
////////////////////////////////////////////////////////////////////////
// Board is shown with fields numbered, user is asked to enter field#
// Computer then makes a move, if at any point user or computer wins
// message is shown, message is also shown if there is a draw (no moves left)
// Game board is shown (with all the moves) and the whole process is repeated
////////////////////////// How to compile/run ////////////////////////////
// Tested on raspberry pi 3 (armv7l)
// $ as -o tic-tac-toe.o tic-tac-toe.asm
// $ gcc -o tic-tac-toe tic-tac-toe.o
// $ ./tic-tac-toe
.data
.balign 4
board_template: .asciz "--------------------------------------------------\n0 | 1 | 2\n  +   +\n3 | 4 | 5\n  +   +\n6 | 7 | 8\n"
new_line: .asciz "\n"
row_sep: .asciz "\n  +   +\n"
col_fmt: .asciz "%c | %c | %c"
move_prompt: .asciz "Your move please: "
move_fmt: .asciz "%d"
congrats: .asciz "Congrats, you won!\n"
draw: .asciz "No winners.\n"
loser: .asciz "What a looser!\n"
new_move: .word 0
user: .word 0
computer: .word 0

.text
.extern printf
.extern scanf
.extern time
.extern srand
.extern rand
.global main
main:

    PUSH {IP, LR}
    // initialize random number generator srand(time(NULL))
    MOV R0, #0
    BL time
    BL srand

next_round:
    LDR R0, =board_template
    BL printf               // show field numbers

ask_move:
    LDR R0, =move_prompt
    BL printf               // ask for move

    LDR R0, =move_fmt
    LDR R1, =new_move
    BL scanf                // accept user's move

    LDR R1, =new_move
    LDR R1, [R1]
    BL is_move_valid
    CMP R0, #0
    BEQ ask_move            // if move invalid, ask again

    LDR R0, =new_move
    LDR R0, [R0]
    MOV R1, #1
    LSL R1, R1, R0  // create mask based on user input
    LDR R0, =user
    LDR R0, [R0]    // r0 now contains user old moves
    ORR R0, R0, R1  // r0 contains old user moves + new user move.
    LDR R1, =user
    STR R0, [R1]    // save user moves


    LDR R1, [R1]    // load user moves
    BL is_winning_move
    CMP R0, #1
    BEQ user_won    // if user won, notify and end game

    BL are_moves_left   // if no moves left, act accordingly
    CMP R0, #0
    BEQ no_winner

computers_move:
    BL generate_move
    LDR R2, =new_move      // save move
    STR R0, [R2]
    MOV R1, R0
    BL is_move_valid
    CMP R0, #0
    BEQ computers_move      // if invalid, try another

    LDR R0, =computer       // load computer's moves
    LDR R0, [R0]
    LDR R1, =new_move     // load current move
    LDR R1, [R1]

    MOV R2, #1
    LSL R1, R2, R1         // change new move into a mask
    
    ORR R0, R0, R1          // add current move to old ones
    LDR R1, =computer
    STR R0, [R1]            // save

    MOV R1, R0
    BL is_winning_move
    CMP R0, #1
    BEQ user_lost          // if computer won, notify, end

    BL show_game_board

    BL are_moves_left
    CMP R0, #0
    BEQ no_winner

    B next_round

user_won:
    LDR R0, =congrats
    BL printf
    B end_game
no_winner:
    LDR R0, =draw
    BL printf
    B end_game
user_lost:
    LDR R0, =loser
    BL printf
    B end_game
end_game:
    BL show_game_board
    POP {IP, PC}

/////////////////////////////////////////////////////////////////////////////
.global are_moves_left
are_moves_left: // bool(r0) are_moves_left()
                // returns 0 (in r0) if board is full, 1 otherwise
    LDR R0, =user
    LDR R0, [R0]
    LDR R1, =computer
    LDR R1, [R1]
    ORR R0, R0, R1
    MOV R1, #512
    SUB R1, R1, #1  // 511 won't fit as a literal
    CMP R0, R1
    BEQ no_moves
    // returns 1 (there are moves)
    MOV R0, #1
    BX LR

no_moves:
    MOV R0, #0
    BX LR


/////////////////////////////////////////////////////////////////////////////
.global is_move_valid
is_move_valid:  // bool(r0) is_move_valid(int r1)
                // r1 contains number of square where user wants to place X
                // if move is valid returns 1, else 0 (in r0)
    CMP R1, #8
    BGT move_invalid    // if greather than 8 then invalid
    LDR R2, =user
    LDR R2, [R2]
    LDR R3, =computer
    LDR R3, [R3]
    ORR R2, R2, R3  // r2 now contains all used moves
    MOV R3, #1
    LSL R3, R3, R1  // r3 = 1 << r1 (change selected numeric value to bitmask)
    AND R3, R3, R2
    CMP R3, #0      // if r3 == 0 the move is valid
    BEQ move_valid
move_invalid:
    MOV R0, #0
    BX LR
move_valid:
    MOV R0, #1
    BX LR

/////////////////////////////////////////////////////////////////////////////
.global show_game_board
show_game_board:    // display game board
    PUSH {IP, LR}

    LDR R1, =user
    LDR R1, [R1]
    AND R1, R1, #1
    CMP R1, #1
    BEQ f0_x        // if user occupies first field, set X there

    LDR R1, =computer
    LDR R1, [R1]
    AND R1, R1, #1
    CMP R1, #1
    BEQ f0_o        // if computer occupies first field, set O there
    
    MOV R1, #32
    B f0_done       // otherwise put space in first field
f0_x:
    MOV R1, #88   // put X as first char
    B f0_done
f0_o:
    MOV R1, #79   // put O as first char
    B f0_done
f0_done:

    LDR R2, =user
    LDR R2, [R2]
    AND R2, R2, #2
    CMP R2, #2
    BEQ f1_x

    LDR R2, =computer
    LDR R2, [R2]
    AND R2, R2, #2
    CMP R2, #2
    BEQ f1_o

    MOV R2, #32
    B f1_done

f1_x:
    MOV R2, #88
    B f1_done
f1_o:
    MOV R2, #79
    B f1_done
f1_done:

    LDR R3, =user
    LDR R3, [R3]
    AND R3, R3, #4
    CMP R3, #4
    BEQ f2_x

    LDR R3, =computer
    LDR R3, [R3]
    AND R3, R3, #4
    CMP R3, #4
    BEQ f2_o

    MOV R3, #32
    B f2_done

f2_x:
    MOV R3, #88
    B f2_done
f2_o:
    MOV R3, #79
    B f2_done
f2_done:
    LDR R0, =col_fmt
    BL printf       // print first row
    LDR R0, =row_sep
    BL printf

// line 2
    LDR R1, =user
    LDR R1, [R1]
    AND R1, R1, #8
    CMP R1, #8
    BEQ f00_x

    LDR R1, =computer
    LDR R1, [R1]
    AND R1, R1, #8
    CMP R1, #8
    BEQ f00_o
    
    MOV R1, #32
    B f00_done
f00_x:
    MOV R1, #88
    B f00_done
f00_o:
    MOV R1, #79
    B f00_done
f00_done:

    LDR R2, =user
    LDR R2, [R2]
    AND R2, R2, #16
    CMP R2, #16
    BEQ f11_x

    LDR R2, =computer
    LDR R2, [R2]
    AND R2, R2, #16
    CMP R2, #16
    BEQ f11_o

    MOV R2, #32
    B f11_done

f11_x:
    MOV R2, #88
    B f11_done
f11_o:
    MOV R2, #79
    B f11_done
f11_done:

    LDR R3, =user
    LDR R3, [R3]
    AND R3, R3, #32
    CMP R3, #32
    BEQ f22_x

    LDR R3, =computer
    LDR R3, [R3]
    AND R3, R3, #32
    CMP R3, #32
    BEQ f22_o

    MOV R3, #32
    B f22_done

f22_x:
    MOV R3, #88
    B f22_done
f22_o:
    MOV R3, #79
    B f22_done
f22_done:
    LDR R0, =col_fmt
    BL printf       // print second row
    LDR R0, =row_sep
    BL printf

// line 3
    LDR R1, =user
    LDR R1, [R1]
    AND R1, R1, #64
    CMP R1, #64
    BEQ f000_x

    LDR R1, =computer
    LDR R1, [R1]
    AND R1, R1, #64
    CMP R1, #64
    BEQ f000_o
    
    MOV R1, #32
    B f000_done
f000_x:
    MOV R1, #88
    B f000_done
f000_o:
    MOV R1, #79
    B f000_done
f000_done:

    LDR R2, =user
    LDR R2, [R2]
    AND R2, R2, #128
    CMP R2, #128
    BEQ f111_x

    LDR R2, =computer
    LDR R2, [R2]
    AND R2, R2, #128
    CMP R2, #128
    BEQ f111_o

    MOV R2, #32
    B f111_done

f111_x:
    MOV R2, #88
    B f111_done
f111_o:
    MOV R2, #79
    B f111_done
f111_done:

    LDR R3, =user
    LDR R3, [R3]
    AND R3, R3, #256
    CMP R3, #256
    BEQ f222_x

    LDR R3, =computer
    LDR R3, [R3]
    AND R3, R3, #256
    CMP R3, #256
    BEQ f222_o

    MOV R3, #32
    B f222_done

f222_x:
    MOV R3, #88
    B f222_done
f222_o:
    MOV R3, #79
    B f222_done
f222_done:
    LDR R0, =col_fmt
    BL printf       // print last row

    LDR R0, =new_line
    BL printf       // drop line

    POP {IP, PC}


/////////////////////////////////////////////////////////////////////////////
.global is_winning_move
is_winning_move:     // bool(r0) is_winning_move(int r1)
                     // returns 1 (in r0) if r1 has winning configuration
                     // returns 0 (in r0) if r1 does not have winning configuration

    AND R2, R1, #448
    CMP R2, #448
    BEQ chicken_dinner
    AND R2, R1, #56
    CMP R2, #56
    BEQ chicken_dinner
    AND R2, R1, #7
    CMP R2, #7
    BEQ chicken_dinner
    MOV R3, #274
    SUB R3, R3, #1      // 273 won't fit as a literal
    AND R2, R1, R3
    CMP R2, R3
    BEQ chicken_dinner
    AND R2, R1, #84
    CMP R2, #84
    BEQ chicken_dinner
    AND R2, R1, #292
    CMP R2, #292
    BEQ chicken_dinner
    AND R2, R1, #146
    CMP R2, #146
    BEQ chicken_dinner
    AND R2, R1, #73
    CMP R2, #73
    BEQ chicken_dinner

    // no winning config found, return 0
    MOV R0, #0
    BX LR

chicken_dinner:
    MOV R0, #1
    BX LR

/////////////////////////////////////////////////////////////////////////////
.global generate_move
generate_move:    // generate move (random number 0-8) and return it in r0
                  // disclaimer:
                  // I did not know how to perform modulo (rand() % 9)
                  // I wrote it in C and used gcc's compile to assembly option
                  // this function is based on the gcc's output
    PUSH {IP, LR}
    BL rand
    LDR R3, n95
    SMULL R3, R1, R3, R0
    ASR R3, R0, #31
    RSB R3, R3, R1, ASR #1
    ADD R3, R3, R3, LSL #3
    SUB R0, R0, R3

    POP {IP, PC}

n95: .word 954437177
