/*
 * File:   main.c
 * Author: jakubo
 *
 * Created on March 18, 2017, 9:29 AM
 * Description:
 *  Trying to get 16bit working.
 */
/*
// FBS
#pragma config BWRP = OFF               // Boot Segment Write Protect (Disabled)
#pragma config BSS = OFF                // Boot segment Protect (No boot program flash segment)

// FGS
#pragma config GWRP = OFF               // General Segment Write Protect (General segment may be written)
#pragma config GCP = OFF                // General Segment Code Protect (No Protection)

// FOSCSEL
#pragma config FNOSC = FRCDIV           // Oscillator Select (8MHz FRC oscillator With Postscaler (FRCDIV))
//#pragma config SOSCSRC = ANA            // SOSC Source Type (Analog Mode for use with crystal)
//#pragma config LPRCSEL = HP             // LPRC Oscillator Power and Accuracy (High Power, High Accuracy Mode)
//#pragma config IESO = OFF               // Internal External Switch Over bit (Internal External Switchover mode disabled (Two-speed Start-up disabled))

// FOSC
//#pragma config POSCMOD = NONE           // Primary Oscillator Configuration bits (Primary oscillator disabled)
#pragma config OSCIOFNC = IO            // CLKO Enable Configuration bit (Port I/O enabled (CLKO disabled))
//#pragma config POSCFREQ = HS            // Primary Oscillator Frequency Range Configuration bits (Primary oscillator/external clock input frequency greater than 8MHz)
//#pragma config SOSCSEL = SOSCHP         // SOSC Power Selection Configuration bits (Secondary Oscillator configured for high-power operation)
//#pragma config FCKSM = CSDCMD           // Clock Switching and Monitor Selection (Both Clock Switching and Fail-safe Clock Monitor are disabled)

// FWDT
//#pragma config WDTPS = PS32768          // Watchdog Timer Postscale Select bits (1:32768)
//#pragma config FWPSA = PR128            // WDT Prescaler bit (WDT prescaler ratio of 1:128)
#pragma config FWDTEN = OFF             // Watchdog Timer Enable bits (WDT disabled in hardware; SWDTEN bit disabled)
#pragma config WINDIS = OFF             // Windowed Watchdog Timer Disable bit (Standard WDT selected(windowed WDT disabled))

// FPOR
#pragma config BOREN = BOR0             // Brown-out Reset Enable bits (Brown-out Reset disabled in hardware, SBOREN bit disabled)
#pragma config RETCFG = OFF             //  (Retention regulator is not available)
//#pragma config PWRTEN = ON              // Power-up Timer Enable bit (PWRT enabled)
//#pragma config I2C1SEL = PRI            // Alternate I2C1 Pin Mapping bit (Use Default SCL1/SDA1 Pins For I2C1)
//#pragma config BORV = V18               // Brown-out Reset Voltage bits (Brown-out Reset set to lowest voltage (1.8V))
//#pragma config MCLRE = OFF              // MCLR Pin Enable bit (RA5 input pin enabled, MCLR disabled)

// FICD
//#pragma config ICS = PGx1               // ICD Pin Placement Select bits (EMUC/EMUD share PGC1/PGD1)


 */ 
#pragma config OSCIOFNC = IO 
#include <xc.h>

//#define _XTAL_FREQ  20000000



#define LED0 PORTAbits.RA2
#define LED1 PORTAbits.RA3

int main(void) {

    ANSA = 0x0000; //analog
    TRISA = 0x0000; //output
    
    while(1){
        PORTA = 0xFFFF;
        unsigned int i;
        for(i=0;i<4000;i++);
        PORTA = 0x0000;
        for(i=0;i<4000;i++);
    } 
}
